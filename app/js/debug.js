class DebugHud
{
    constructor( container )
    {
        this.content = [];
        this.idx = 0;
        this.container = null;

    }

    addLine( text )
    {
        this.content[ this.idx ] = text;
        this.idx++;
    }

    flush( visible = true )
    {
        if( this.container == null ) return;

        if( !visible ){
            this.container.innerHTML = "";
        }
        else{
            let text = "DebugHud";
            for( let i = 0; i < this.content.length; i++ )
            {
                text += "<br>" + this.content[i];
            }
            this.container.innerHTML = text;
        }
        
        this.content = [];
        this.idx = 0;
    }
}

const debugHud = new DebugHud(); 

module.exports = {
    debugHud
};